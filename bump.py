import argparse

SOURCE_FILE = 'helm-values.yaml'

def bump(repository, tag):
    bumped_config = ""

    # Read file, building a list of lines
    with open(SOURCE_FILE) as file:
        lines = file.readlines()

    image_string = "repository: " + repository
    bump_next_tag = False
    
    # Assuming that image repository is always set before the tag, won't work otherwise
    for line in lines:

        # If a matching image is found, setting a flag, so next tag will be bumped
        if image_string in line:
            bump_next_tag = True

        # if tag line and bump flag is set, bump that tag
        if "tag:" in line and bump_next_tag:        
            bump_next_tag = False
            split = line.split("tag: ")
            bumped_config = bumped_config + split[0] + "tag: " + args.tag + "\n"
        else:
            # Else just copy the line to the output config
            bumped_config = bumped_config + line

    # Write new config back to file
    with open(SOURCE_FILE, 'r+') as file:
        file.seek(0)
        file.write(bumped_config)
        file.truncate()

if __name__ == "__main__":
    # Handle arguments and call the bump function
    parser = argparse.ArgumentParser()
    parser.add_argument("--repository", help="repository to bump", required=True)
    parser.add_argument("--tag", help="new tag to use", required=True)
    args = parser.parse_args()

    bump(args.repository, args.tag)