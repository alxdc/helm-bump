values_file="helm-values.yaml"

# Use grep to find all occurrences (and line numbers) of the repository
repository=$(grep -n $1 $values_file)

# Keep matching line numbers only
repository_line=$(echo "$repository" | cut -d: -f1)

# Loop through matching line numbers
while read -r line
do
    # Starting at the matching line number, search for the tag keyword, and get the offset line number
    tag_line_offset=$(tail -n +$line $values_file | grep -n -m1 tag | cut -d: -f1)

    # Calculate the absolute line number of the corresponding tag keyword
    tag_line=$(($line+$tag_line_offset-1))

    # Replace version number with the new version passed as argument
    sed -i "${tag_line}s/: .*/: $2/" helm-values.yaml
    
done <<< "$repository_line"
